module.exports = function(grunt) {
    grunt.loadNpmTasks("grunt-ts");
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-open');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-path');
    grunt.loadNpmTasks('grunt-browserify');

    //require('load-grunt-tasks')(grunt);

    grunt.registerTask("default", ["ts"]);


    var SOURCE_FILES = [
        'src/*.ts'
    ]


    grunt.initConfig({

        requirejs: {
            compile: {
                options: {
                    baseUrl: ".",
                    mainConfigFile: "mainConfigFile.js",
                    name: "default",
                    include: ["build/main.js"],
                    out: "build/optimized.js"
                }
            }
        },


        concat: {
            dist: {
                src: ['src/app/javascript/*.js'],
                dest: './compiled.js',
            },
        },

        pkg: grunt.file.readJSON('package.json'),
        connect: {
            server: {
                options: {
                    port: 8080,
                    base: './',
                    livereload: true
                }
            }
        },
        ts: {

            default: {

                src: SOURCE_FILES,
                outDir: '_build',
                options: {
                    target: 'es5'
                }
            }
        },

        browserify: {
            js: {
                src: '_build/main.js',
                dest: 'build/main.js',
            }
        },

        watch: {
          assets: {

              // Assets to watch:
              files: ['src/*.ts',],
              tasks: ['ts' , 'browserify']
          },
          livereload: {
              options: {
                  livereload: true
              },
              files: [
                  'src/**/*'
              ]
          },
          options: {
              interrupt: true,
              debounceDelay: 250,
              reload: true,
              livereload: true
          },
        },
        open: {
            dev: {
                path: 'http://localhost:8080/index.html'
            }
        }
    });


    grunt.registerTask('default', ["ts", 'browserify', 'connect', 'open', 'watch']);

}
